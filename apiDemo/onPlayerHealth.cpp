#include "events.h"
#include "apiPlayer.h"

void onPlayerHpChanged(int playerid, int agentType, int agentId)
{
	if(agentType == 0) //Damage was caused by a player
	{
		if(apiPlayer::isOn(agentId)) //The player is online
		{
			char buffer[50] = "";
			sprintf_s(buffer, "Damaged by %s", apiPlayer::get(agentId).getNick());
			apiPlayer::get(playerid).sendMsg(buffer, 0xFFFF0000);
		}
	}
	else if(agentType == 1) //Damaged was caused by a vehicle
	{
	}
	else //Unable to determine what caused the HP change
	{
	}
}