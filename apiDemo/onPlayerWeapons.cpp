#include "events.h"
#include "apiPlayer.h"

bool onPlayeChangeWeapon(int playerid, int weapon, int ammo)
{
	if(weapon == 18) return false; //Returning false will block the change
	return true;
}

bool onPlayerWeaponsUpdated(int playerid)
{
	apiPlayer::player& p = apiPlayer::get(playerid);

	char buffer[40] = "";
	int bWeapon = 0, bAmmo = 0;

	unsigned int size = p.getWeaponsSize();
	for(unsigned int i = 0; i < size; i++)
	{
		p.getWeaponData(i, bWeapon, bAmmo);
		sprintf_s(buffer, "Weapon: %i, Ammo: %i", bWeapon, bAmmo);
		p.sendMsg(buffer, 0xFFFFFFFF);
	}
	return true;
}
