#include "events.h"
#include "apiPlayer.h"
#include <iostream>
#include "apiMath.h"
#include "playerList.h"

bool onPlayerConnect(int playerid)
{
	//Player class only contains default values and the player IP address at this point
	std::cout << "Player slot " << playerid << " is being requested by IP: " << apiPlayer::get(playerid).getIp() << std::endl;
	return true;
}

bool onPlayerCredentials(int playerid)
{
	//The player is successfully connected here, Player API Class is now functional
	apiPlayer::player& player = apiPlayer::get(playerid);

	//The following code is necessary
	player.setSkin(playerList::getModel(10));
	player.setColor(0xFF0000); //RGB
	player.setWorld(1); //1 is set by default, but you can set any world here
	apiMath::Vector3 spawnPos(-205.476f, 630.067f, 13.8088f);
	player.spawn(spawnPos);
	//Done

	char buffer[50] = "";
	sprintf_s(buffer, "Player %s(%i) has joined the server", apiPlayer::get(playerid).getNick(), playerid);
	apiPlayer::sendMsgToAll(buffer, 0xFFFF0000); //Chat colors are always ARGB
	return true;
}