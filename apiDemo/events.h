bool onPlayerConnect(int playerid);
bool onPlayerCredentials(int playerid);
bool onPlayeChangeWeapon(int playerid, int weapon, int ammo);
bool onPlayerWeaponsUpdated(int playerid);

void onPlayerChat(int playerid, const char* chat);

void onPlayerHpChanged(int playerid, int agentType, int agentId);

void onPlayerDialogResponse(int playerid, unsigned int dialogId, int buttonId, int rowId);