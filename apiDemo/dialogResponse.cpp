#include "events.h"
#include "apiPlayer.h"
#include <iostream>

void onPlayerDialogResponse(int playerid, unsigned int dialogId, int buttonId, int rowId)
{
	std::cout << "Dialog response: player: " << playerid << ", dialog: " << dialogId << 
		", button: " << buttonId << ", row: " << rowId << std::endl;

	if(dialogId == 1 && buttonId == 1 && rowId >= 0 && rowId < 4)
	{
		apiPlayer::get(playerid).giveWeapon(12 + rowId, 500); //UZI weapon ID is 12
	}
}