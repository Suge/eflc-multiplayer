#include <Windows.h>
#include <iostream>

#include <mainNetwork.h>
#include "apiWorld.h"
#include "apiPlayerEvents.h"
#include <time.h>
#include "events.h"
#include "apiVehicleEvents.h"
#include "apiVehicle.h"
#include "apiWorld.h"

void timedFunction()
{
	//Lets make a simple speedometer
	apiMath::Vector3 velocity;
	int currentVehicle = 0;
	float speed = 0.0f;
	char speedoMsg[15] = "";

	//Start looping the players
	for(unsigned int i = 1; i < 17; i++)
	{
		if(apiPlayer::isOn(i))
		{
			currentVehicle = apiPlayer::get(i).getDriving();
			if(currentVehicle != NULL && apiVehicle::isVehicle(currentVehicle))
			{
				velocity = apiVehicle::getVehicle(currentVehicle).getVelocity();
				//Converts the Velocity vector to Speed (miles per hour)
				speed = std::sqrtf(velocity.x * velocity.x + velocity.y * velocity.y + velocity.z * velocity.z) * 1.609f;
				
				sprintf_s(speedoMsg, "Speed: %i", (int)speed);
				apiPlayer::get(i).drawInfoText(speedoMsg, 600);
			}
		}
	}
}

int main()
{
	if(initRaknet(8888, "Server Name", "Server location", "Server WebSite", false))
	{
		std::cout << "Server network was created" << std::endl;

		apiWorld::createWorld(1, 1, 7, 0, 2000); 
		/*Creates world ID 1 with weatherID 1, game time 7 hours
		If the world is not defined then the game hours won't be synced*/

		//Register server callbacks (events)
		apiPlayerEvents::registerPlayerJoin(onPlayerConnect);
		apiPlayerEvents::registerPlayerCredentials(onPlayerCredentials);
		apiPlayerEvents::registerPlayerHpChange(onPlayerHpChanged);
		apiPlayerEvents::registerPlayerWeaponChanged(onPlayeChangeWeapon);
		apiPlayerEvents::registerPlayerWeaponsArrived(onPlayerWeaponsUpdated);
		apiPlayerEvents::registerPlayerChat(onPlayerChat);
		apiPlayerEvents::registerPlayerDialogListResponse(onPlayerDialogResponse);

		//Defines a simple timer
		clock_t lastTimerCall = 0;
		clock_t weatherTimer = 60000;

		apiWorld::createWorld(1, 1, 12, 1, 2000); //Defined a world so the game hours/weather are in synch

		while(true)
		{
			pulseServer(); //Makes the IVMP server pulse

			clock_t currentTime = clock();
			if(currentTime > lastTimerCall)
			{
				lastTimerCall = currentTime + 500; //Next call will be in 500ms
				timedFunction();
			}

			if(currentTime > weatherTimer)
			{
				weatherTimer = currentTime + 60000;

				//This is basically alternating weathers between ids 1 and 7 every 60 seconds
				int nextWeather = apiWorld::getWorld(1)->getWeather() == 1 ? 7 : 1;
				apiWorld::getWorld(1)->setWeather(nextWeather);
				apiWorld::getWorld(1)->streamWorldChanges();
			}

			Sleep(100);
		}
	}
	return 1;
}