#include "apiMath.h"

#ifndef apiVehicle_H
#define apiVehicle_H

namespace apiVehicle
{
	//All data is read only

	class vehicle
	{
		private:
			int id;

		public:
			~vehicle();
			vehicle(int id);

			__declspec(dllexport) apiMath::Vector3 getPosition();
			__declspec(dllexport) apiMath::Vector3 getVelocity();
			__declspec(dllexport) apiMath::Vector3 getRotation();
			__declspec(dllexport) int getId();
			__declspec(dllexport) int getDriver(); //Returns the current driver ID or NULL
			__declspec(dllexport) void setTune(unsigned int part, bool on); //part is 0-9
			__declspec(dllexport) bool isTuned(unsigned int part);
			__declspec(dllexport) void setColor(int color1, int color2, int color3, int color4);
			__declspec(dllexport) int getColor(unsigned int slotId);
			__declspec(dllexport) void setIndicator(unsigned int indicator, bool onOrOff); //indicators are 0-1
			__declspec(dllexport) bool getIndicator(unsigned int indicator);

			__declspec(dllexport) void setPosition(apiMath::Vector3& v);
			__declspec(dllexport) void setRotation(apiMath::Vector3& v);
			__declspec(dllexport) void setVelocity(apiMath::Vector3& v);

			__declspec(dllexport) void setLivery(int livery);
			__declspec(dllexport) void setDirtLevel(float level);

			__declspec(dllexport) int getEngineHealth();
			__declspec(dllexport) void setEngineHealth(int hp, bool fixVisual);

			__declspec(dllexport) bool getTire(unsigned int index); //Index is 0-5
			__declspec(dllexport) void setTire(unsigned int index, bool status);

			__declspec(dllexport) float getGasPedal();

			__declspec(dllexport) void setEngineFlags(int flags); //0 off, 1 off but startable, 3 on
			__declspec(dllexport) int getEngineFlags();
	};
	
	//Vehicle model id (mID) must be valid otherwise the server/client may crash
	__declspec(dllexport) int addVehicle(int mId, apiMath::Vector3 pos, apiMath::Vector3 rot, int c1, int c2, int c3, int c4, unsigned int vWorld);
	__declspec(dllexport) bool deleteVehicle(int vehicleId); //Throws exception
	__declspec(dllexport) bool isVehicle(int vehicleId);
	__declspec(dllexport) vehicle& getVehicle(int vehicleId); //Throws exception
}

#endif