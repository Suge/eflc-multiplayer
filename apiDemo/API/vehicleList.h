#include <vector>

namespace vehicleList
{
	//Internal usage only
	void loadList();
	void getDefaultTune(unsigned int i, size_t& defaultSize, std::vector<int>& tunes);
	unsigned long getModel(int id);

	//API and Internal
	__declspec(dllexport) bool isValidModel(int id);
	__declspec(dllexport) int getIdByName(const char* carName);
	__declspec(dllexport) const char* getModelName(int id);
}