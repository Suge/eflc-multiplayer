#include "apiMath.h"

#ifndef APIBLIPS_H
#define APIBLIPS_H

namespace apiBlips
{
	//For the full list of blip types see this http://public.sannybuilder.com/GTA4/blips/ 
	__declspec(dllexport) int add(apiMath::Vector3 pos, int type, unsigned int color, unsigned int world, unsigned int streamDistance, bool showAll);
	__declspec(dllexport) void setName(int id, const char* name);
	__declspec(dllexport) void remove(int id);
}

#endif