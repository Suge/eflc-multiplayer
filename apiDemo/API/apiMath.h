#ifndef apimath_h
#define apimath_h

namespace apiMath
{
	struct Vector3
	{	
		float x;
		float y;
		float z;

		__declspec(dllexport) void empty(); //sets values to 0.0
		__declspec(dllexport) Vector3();
		__declspec(dllexport) Vector3(float x, float y, float z);
	};

	struct Quaternion
	{
		float Y;
		float Z;
		float X;
		float W;

		__declspec(dllexport) void init(float tx, float ty, float tz, float tw);
		__declspec(dllexport) float Length();

		__declspec(dllexport) Quaternion(float tx, float ty, float tz, float tw);
		__declspec(dllexport) Quaternion();
	};

	__declspec(dllexport) float distance3d(Vector3& vecOne, Vector3& vecTwo);

	__declspec(dllexport) void slerp(Quaternion& copyVec, Quaternion& left, Quaternion& right, float amount);
	
	__declspec(dllexport) Vector3 vecLerp(Vector3 v1, Vector3 v2, float progress);

	__declspec(dllexport) float lerp(float x, float xx, float progress);

	__declspec(dllexport) float fSlerp(float f1, float f2, float progress);

	__declspec(dllexport) float GetOffsetDegrees(float a, float b);

	__declspec(dllexport) void secondsToMinsSecs(int seconds, int& nmins, int& nsecs);

	__declspec(dllexport) Quaternion toQuaternion(float pitch, float roll, float yaw);
	__declspec(dllexport) Vector3 fromQuaternion(Quaternion& q);
}

#endif