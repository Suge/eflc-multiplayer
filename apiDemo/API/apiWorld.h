#ifndef APIWORLD_H
#define APIWORLD_H

namespace apiWorld
{
	class virtualWorld
	{
		private:
			unsigned int id;
			unsigned int weather;
			unsigned int hour;
			float minutes;
			unsigned int minuteDuration; //In microsecs

		public:
			virtualWorld(unsigned int id, unsigned int weather, unsigned int hour, unsigned int minutes, unsigned int minuteDuration);
			~virtualWorld();

			void updateWorld(long cTime);

			__declspec(dllexport) void setWeather(unsigned int weather);
			__declspec(dllexport) void setTime(unsigned int hour, unsigned int minutes);
			__declspec(dllexport) void setMinuteDuration(unsigned int duration);
			__declspec(dllexport) void streamWorldChanges();

			__declspec(dllexport) void getTime(unsigned int& hour, unsigned int& minutes);
			__declspec(dllexport) unsigned int getWeather();
			__declspec(dllexport) unsigned int getMinuteDuration();
	};

	__declspec(dllexport) bool isWorld(unsigned int id);
	__declspec(dllexport) void createWorld(unsigned int id, unsigned int weather, unsigned int hour, unsigned int minutes, unsigned int minuteDuration);
	__declspec(dllexport) void deleteWorld(unsigned int id); //Throws exception

	__declspec(dllexport) virtualWorld* getWorld(unsigned int id); //Throws exception
}

#endif