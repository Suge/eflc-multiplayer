namespace apiVehicleEvents
{	
	__declspec(dllexport) typedef void(*vHealth)(int vehicleId, int health);
	__declspec(dllexport) void registerHealthChange(vHealth f);

	__declspec(dllexport) typedef void(*vTyre)(int vehicleId, unsigned int tyre);
	__declspec(dllexport) void registerTyrePop(vTyre f);
}