#include "apiMath.h"
#include <vector>

#ifndef apiPlayer_H
#define apiPlayer_H

namespace apiPlayer
{
	class player
	{
		private:
			unsigned long long engineId;

		public:
			player(unsigned long long engineId);
			~player();

			__declspec(dllexport) apiMath::Vector3 getPos();
			__declspec(dllexport) void setPos(apiMath::Vector3& pos);
			__declspec(dllexport) int getID();

			__declspec(dllexport) const char* getNick();
			__declspec(dllexport) void setNick(const char* nick);

			__declspec(dllexport) void sendMsg(const char* msg, unsigned int hex);
			__declspec(dllexport) void sendColoredMsg(int msgId);

			__declspec(dllexport) void setSkin(unsigned int model);

			__declspec(dllexport) void setWorld(unsigned int vWorld);
			__declspec(dllexport) unsigned int getWorld();

			__declspec(dllexport) void giveWeapon(unsigned int weaponId, unsigned int ammo);
			__declspec(dllexport) void removeWeapon(unsigned int weaponId); // Use weapon ID 0 to remove all weapons

			__declspec(dllexport) void setHealth(int hp);
			__declspec(dllexport) int getHealth(); //Ranges from 0-200 when the player is alive, negative values if dead

			__declspec(dllexport) void setColor(unsigned int hex); //RGB
			__declspec(dllexport) unsigned int getColor();

			__declspec(dllexport) void spawn(apiMath::Vector3& pos); //Must be used when the credentials arrive
			__declspec(dllexport) void setRespawnPos(apiMath::Vector3& pos); //Must be used to set a new position only

			__declspec(dllexport) int getDriving(); //Returns 0 if on foot
			__declspec(dllexport) void getPassenger(int& vehicle, int& seat);
			__declspec(dllexport) int isInVehicle(); //Checks for driving and passenger or returns 0 if on foot
			__declspec(dllexport) void putInVehicle(int vehicleId, int seatId); //Seats: 0 driver, 1-3 passengers
			__declspec(dllexport) void removeFromVehicle();

			//drawText time should be 0 for infinite
			__declspec(dllexport) void drawText(int classId, float x, float y, float sx, float sy, const char* txt, int time, unsigned int color);
			__declspec(dllexport) void updateText(int classId, const char* txt, unsigned int newColor);
			__declspec(dllexport) void drawInfoText(const char* txt, int time);
			__declspec(dllexport) void drawRect(int classId, float x, float y, float sx, float sy, unsigned int color);
			__declspec(dllexport) void wipeDrawClass(int classId);

			__declspec(dllexport) int getWeapon();
			__declspec(dllexport) int getAmmo();

			__declspec(dllexport) void setFrozen(bool b);

			__declspec(dllexport) void debugPlayer(int playerid);
			__declspec(dllexport) void debugVehicle(int vehicleid);

			__declspec(dllexport) void disconnect();

			__declspec(dllexport) int getPing(bool average);

			__declspec(dllexport) int getAnim();

			__declspec(dllexport) void showDialogList(unsigned int dialogId);

			__declspec(dllexport) void setClothes(unsigned int part, unsigned int value); //part is 0-8
			__declspec(dllexport) void setProperty(unsigned int part, unsigned int value); //part is 0-1

			__declspec(dllexport) void forceAnim(const char* animGroup, const char* animName, bool loop);
			__declspec(dllexport) void stopAnims();

			__declspec(dllexport) void setKeyHook(unsigned int virtualKey, bool addOrRemove); //Virtual keys can be found online

			__declspec(dllexport) const char* getIp();

			__declspec(dllexport) void showBlip(int blipId, bool show);

			__declspec(dllexport) void cam_setPos(apiMath::Vector3* pos, unsigned int camWorld);
			__declspec(dllexport) void cam_setLookAt(apiMath::Vector3 pos);
			__declspec(dllexport) void cam_attachOnPlayer(int playerid);

			unsigned int getSerialSize(); //Returns the amount of serials
			const char* getSerial(unsigned int i);

			__declspec(dllexport) void closeMods();
			__declspec(dllexport) void checkSum(const char* file); //The result will arrive as a callback for registerPlayerCheckSum

			__declspec(dllexport) bool isObjectSpawned(int id); //Returns whether the player has spawned the object
			__declspec(dllexport) bool isVehicleSpawned(int id);

			__declspec(dllexport) void requestWeapons(); //The result will arrive as a callback for registerPlayerWeaponsArrived
			__declspec(dllexport) unsigned int getWeaponsSize();
			__declspec(dllexport) void getWeaponData(unsigned int index, int& weapon, int& ammo); //Index throws exception, use getWeaponsSize for array size
			
			__declspec(dllexport) unsigned int getRoomKey();

			__declspec(dllexport) void playStream(const char* stream, apiMath::Vector3* pos); //Use NULL as pos for a frontend stream
			__declspec(dllexport) void stopStreams();

			__declspec(dllexport) void setArmor(int i);
			__declspec(dllexport) int getArmor();

			__declspec(dllexport) void setMoney(int i);
			__declspec(dllexport) void setDoorStat(unsigned int model, apiMath::Vector3* pos, bool status);
	};

	__declspec(dllexport) void sendMsgToAll(const char* msg, unsigned int hex);
	__declspec(dllexport) void sendColoredMsgToAll(int msgId);

	__declspec(dllexport) player& get(int id); //Throws exception
	__declspec(dllexport) bool isOn(int id);

	void registerResourcesSite(const char* sitePath);
	void registerResource(const char* fileName);
}

#endif