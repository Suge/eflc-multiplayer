#include "apiMath.h"
#include <vector>

#ifndef apiDialog_H
#define apiDialog_H

namespace apiDialog
{
	class dialogList
	{
		private:
			unsigned int id;
			char* windowName;
			unsigned int columns;
			char* headers;
			char* btnNames[2];

		public:
			dialogList(unsigned int id, char* windowName, unsigned int columns);
			~dialogList();

			__declspec(dllexport) void setColumnsHeaders(const char* s);
			__declspec(dllexport) void setBtnNames(const char* b1, const char* b2);
			__declspec(dllexport) void addRow(const char* s);
			__declspec(dllexport) void clearRows();
			__declspec(dllexport) void save();
	};

	__declspec(dllexport) bool addNew(unsigned int id, char* windowName, unsigned int columns);
	__declspec(dllexport) void remove(int id);
	__declspec(dllexport) bool isValid(int id);
	__declspec(dllexport) dialogList* get(int id);
}

#endif