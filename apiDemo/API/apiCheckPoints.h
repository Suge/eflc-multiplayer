#include "apiMath.h"

namespace apiCheckPoint
{
	class checkPoint
	{
		private:
			apiMath::Vector3 pos;
			float radius;
			unsigned int hexColor;
			unsigned int vWorld;
			int type;
			int blipType;
			
		public:
			checkPoint(apiMath::Vector3 pos, float radius, unsigned int hexColor, int type, int blipType, unsigned int vWorld);
			~checkPoint();

			__declspec(dllexport) apiMath::Vector3 getPos();
			__declspec(dllexport) float getRadius();
			__declspec(dllexport) int getType();
			__declspec(dllexport) unsigned int getWorld();
			__declspec(dllexport) unsigned int getColor();
			__declspec(dllexport) int getBlip();
	};

	__declspec(dllexport) int addNew(apiMath::Vector3 pos, float radius, unsigned int hexColor, int type, int blipType, unsigned int vWorld); //Hex is RGBA
	__declspec(dllexport) void remove(int id);
	__declspec(dllexport) bool isValid(int id);

	__declspec(dllexport) checkPoint* get(int id);

	__declspec(dllexport) void setBlipTypeForPlayer(int cpId, int player, int type);
	__declspec(dllexport) void setShowingForPlayer(int cpId, int player, bool showing);
}