#include "apiPlayer.h"

namespace apiPlayerInputs
{
	__declspec(dllexport) typedef void(*keyInput)(apiPlayer::player& player, unsigned int virtualKey, bool keyUp);
	//Will be called once the player presses the virtualKey, it must be registered with apiPlayer::registerKeyHook(...)
	__declspec(dllexport) void registerKeyInputs(keyInput f);
}