namespace playerList
{
	void loadList();

	__declspec(dllexport) bool isValidModel(int i);
	__declspec(dllexport) unsigned long getModel(int i);
	__declspec(dllexport) int getIdByName(const char* carName);
	__declspec(dllexport) const char* getModelName(int i);
}