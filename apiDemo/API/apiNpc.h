#include "apiMath.h"

namespace apiNpc
{
	__declspec(dllexport) int createNpc(const char* nick, int skinModel, apiMath::Vector3 position, int vehicleId, const char* fileName, unsigned int vWorld);
	__declspec(dllexport) void deleteNpc(int npcId);

	__declspec(dllexport) void clearNpcTaks(int npc);
	__declspec(dllexport) void setNpcStartPos(int npc, apiMath::Vector3 newPos, float newHeading);
	__declspec(dllexport) void addFootTask(int npc, apiMath::Vector3 gotoPos, apiMath::Vector3 aim, float gotoHeading, 
		int anim, int weapon, int taskDuration);

	__declspec(dllexport) void addVehicleTask(int npc, int vehicleId, apiMath::Vector3 pos, apiMath::Vector3 rot,
		apiMath::Vector3 velo, int vehAnim, float pedals, float steer, int duration);
	__declspec(dllexport) void playTasks(int npc, bool repeats);

	__declspec(dllexport) bool tasksPlaying(int npc);

	__declspec(dllexport) typedef void(*tasksFinished)(int npc, bool willRepeat);
	__declspec(dllexport) void registerTasksFinished(tasksFinished f);
}