#include <string>

namespace apiColoredChat
{
	__declspec(dllexport) void create(int id);
	__declspec(dllexport) void dispose(int id);
	__declspec(dllexport) bool isValid(int id);

	__declspec(dllexport) void addMsg(int id, const char* msg, unsigned int hexColor); //Hex is ARGB
	
	unsigned int getMsgsSize(int id);
	void copyMsg(int msgId, int msgSubId, std::string& msg, unsigned int& hexColor);
}