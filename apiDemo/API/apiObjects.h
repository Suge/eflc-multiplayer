#include "apiMath.h"

#ifndef apiObjects_H
#define apiObjects_H

namespace apiObjects
{
	class object
	{
		private:
			apiMath::Vector3 pos;
			apiMath::Quaternion rot;

			unsigned int objHex;
			unsigned int vWorld;

			unsigned int interior;
			int alpha;
			bool hasOffSet;

		public:
			object(apiMath::Vector3 pos, apiMath::Quaternion rot, unsigned int objHex, 
				unsigned int vWorld, bool hasOffset, unsigned int interior, int alpha);
			~object();

			__declspec(dllexport) apiMath::Vector3 getPos();
			__declspec(dllexport) apiMath::Quaternion getRot();
			__declspec(dllexport) unsigned int getWorld();
			__declspec(dllexport) unsigned int getModel();

			bool hasOffset();
			unsigned int getInterior();
			int getAlpha();

			void setPos(apiMath::Vector3 pos);
			void setRot(apiMath::Quaternion rot);
	};

	__declspec(dllexport) int addNew(apiMath::Vector3 pos, apiMath::Quaternion rot, unsigned int objHex, 
		unsigned int vWorld, bool hasOffset, unsigned int interior, int alpha);
	__declspec(dllexport) void remove(int id);
	__declspec(dllexport) bool isValid(int id);
	__declspec(dllexport) object* get(int id); //Throws exception

	__declspec(dllexport) void moveObject(int id, apiMath::Vector3 pos, apiMath::Quaternion rot, unsigned int time);

	__declspec(dllexport) void setAudio(int id, char* audioName, int offset);
}

#endif