#include "apiPlayer.h"

#ifndef apiPlayerEvents_H
#define apiPlayerEvents_H

namespace apiPlayerEvents
{
	__declspec(dllexport) typedef void(*vLeft)(int playerid, int vehicleId, int seatId);
	__declspec(dllexport) typedef void(*vEnterRequest)(int playerid, int vehicleId, int seatId, bool carjack);
	__declspec(dllexport) void registerLeftVehicle(vLeft f);
	__declspec(dllexport) void registerEnteringVehicle(vEnterRequest f);
	__declspec(dllexport) void registerEnteredVehicle(vLeft f);

	__declspec(dllexport) typedef bool(*pJoin)(int playerid);
	__declspec(dllexport) void registerPlayerCredentials(pJoin f);
	__declspec(dllexport) void registerPlayerJoin(pJoin f); //Triggered when the player requests the connection, can be used to check their IP

	__declspec(dllexport) typedef void(*pLeft)(int playerid, int reason); //Reasons: 0 disconnect, 1 timeout
	__declspec(dllexport) void registerPlayerLeft(pLeft f);

	__declspec(dllexport) typedef bool(*pWeap)(int playerid, int weapon, int ammo);
	__declspec(dllexport) void registerPlayerWeaponChanged(pWeap f);

	__declspec(dllexport) typedef void(*pChat)(int playerid, const char* chat);
	__declspec(dllexport) void registerPlayerChat(pChat f);

	__declspec(dllexport) typedef void(*pAfk)(int playerid, bool status);
	__declspec(dllexport) void registerPlayerAfk(pAfk f);

	__declspec(dllexport) typedef void(*pDeath)(int playerid, int agentType, int agentId); //Agent types: 0 player, 1 vehicle, other unknown
	__declspec(dllexport) void registerPlayerDeath(pDeath f);
	__declspec(dllexport) void registerPlayerHpChange(pDeath f);
	__declspec(dllexport) void registerPlayerArmor(pLeft f);

	__declspec(dllexport) void registerPlayerRespawn(pJoin f); //Called once the player respawns after death

	__declspec(dllexport) typedef void(*pCheckPoint)(int playerid, int checkPointId); //The ID can be invalid
	__declspec(dllexport) void registerPlayerEnterCP(pCheckPoint f);
	__declspec(dllexport) void registerPlayerExitCP(pCheckPoint f);

	__declspec(dllexport) typedef void(*pDialogListResponse)(int playerid, unsigned int dialogId, int buttonId, int rowId);
	__declspec(dllexport) void registerPlayerDialogListResponse(pDialogListResponse f);

	_declspec(dllexport) typedef void(*checkSumResponse)(int playerid, const char* path, unsigned int sum);
	__declspec(dllexport) void registerPlayerCheckSum(checkSumResponse f); //Triggered by apiPlayer::checkSum(...)

	__declspec(dllexport) void registerPlayerWeaponsArrived(pJoin f); //Triggered by apiPlayer::requestWeapons()
}

#endif