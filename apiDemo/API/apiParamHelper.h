#include <string>
#include <vector>

#ifndef apiparamhelper_h
#define apiparamhelper_h

namespace apiParamHelper
{
	__declspec(dllexport) int isInt(const char* s);
	__declspec(dllexport) unsigned long isUnsignedInt(const char*, bool allowHex);

	__declspec(dllexport) float isFloat(const char* s);
}

#endif