#include "events.h"
#include "apiPlayer.h"
#include "apiVehicle.h"
#include "apiDialog.h"
#include "vehicleList.h"
#include "playerList.h"

bool isChatMsgACommand(int playerid, const char* chat)
{
	if(chat[0] == '/')
	{
		if(strcmp(chat, "/infernus") == 0)
		{
			//Creating a red INFERNUS at the player position on virtual world 1
			int infernus = apiVehicle::addVehicle(vehicleList::getIdByName("INFERNUS"), apiPlayer::get(playerid).getPos(),
				apiMath::Vector3(), 29, 29, 29, 29, 1);

			if(infernus != -1) //Vehicle was created
			{
				apiPlayer::get(playerid).sendMsg("Infernus spawned", 0xFFFFFFFF);
			}
		}
		else if(strcmp(chat, "/swat") == 0)
		{
			apiPlayer::player& player = apiPlayer::get(playerid); //You can also create a reference for faster access
			player.setSkin(playerList::getModel(201)); //Noose skin
			player.giveWeapon(15, 500); //Gives M4
		}
		else if(strcmp(chat, "/guns") == 0)
		{
			if(!apiDialog::isValid(1))
			{
				//The dialog ID 1 doesnt exist, create it!!
				char dialogWindowName[] = "Spawn a weapon";
				apiDialog::addNew(1, dialogWindowName, 1); //Creates a dialog class
				apiDialog::get(1)->setBtnNames("Spawn", "Close");

				const char* weaps[4] = {"UZI", "SMG", "AK-47", "M4"};
				for(unsigned int i = 0; i < 4; i++)
				{
					apiDialog::get(1)->addRow(weaps[i]);
				}
				apiDialog::get(1)->save(); //Always save after adding/removing rows
			}
			apiPlayer::get(playerid).showDialogList(1); //Open file "dialogResponse.cpp" for the continuation
		}
		else if(strcmp(chat, "/invent") == 0)
		{
			apiPlayer::get(playerid).requestWeapons(); //Open file "onPlayerWeapons.cpp" for the continuation
		}
		else
		{
			apiPlayer::get(playerid).sendMsg("Invalid command", 0xFFFFFFFF);
		}
		return true;
	}
	return false;
}

void onPlayerChat(int playerid, const char* chat)
{
	if(isChatMsgACommand(playerid, chat)) return;

	//What if it wasnt a command?
	char playerChat[130] = "";
	sprintf_s(playerChat, "%s: %s", apiPlayer::get(playerid).getNick(), chat);
	
	apiPlayer::sendMsgToAll(playerChat, 0xFFFFFFFF); //Chat color is always ARGB
}